#include <stdio.h>


/*
1 + 2 = 3
0 + 0 = 0
*/
int mysum(int a, int b) {
  return a + b;
}

int main() {
  int result = 0;
  printf("Hello, my world!\n");

  // 1st test case 
  result = mysum(1, 2);
   if (result == 3) {  
      printf("success\n");
  } else {
      printf("fail\n");
  }

  // 2nd test case
  result = mysum(0, 0);
  if (result == 0) {
      printf("success\n");
  } else {
      printf("fail\n");
  }
  return 0;
}

